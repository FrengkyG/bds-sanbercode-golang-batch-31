package models

import "time"

type (
	Nilai struct {
		ID           uint
		Indeks       string
		Skor         uint      `json:"nilai"`
		MahasiswaId  uint      `json:"mahasiswaId"`
		MataKuliahId uint      `json:"mataKuliahId"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
	}
)
