package nilai

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"
	"tugas15/config"
	"tugas15/models"
)

const (
	table          = "nilai"
	layoutDateTime = "2006-01-02 15:04:05"
)

func GetAll(ctx context.Context) ([]models.Nilai, error) {
	var kumpNilai []models.Nilai
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var nilai models.Nilai
		var createdAt, updatedAt string
		if err = rowQuery.Scan(&nilai.ID,
			&nilai.Indeks,
			&nilai.Skor,
			&nilai.MahasiswaId,
			&nilai.MataKuliahId,
			&createdAt,
			&updatedAt); err != nil {
			return nil, err
		}

		nilai.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		nilai.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		if err != nil {
			log.Fatal(err)
		}

		kumpNilai = append(kumpNilai, nilai)
	}
	return kumpNilai, nil
}

func Insert(ctx context.Context, nilai models.Nilai) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("INSERT INTO %v (indeks, skor, mahasiswa_id, mata_kuliah_id , created_at, updated_at) values('%v',%v, %v,%v, NOW(), NOW())", table,
		nilai.Indeks,
		nilai.Skor,
		nilai.MahasiswaId,
		nilai.MataKuliahId)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

func Update(ctx context.Context, nilai models.Nilai, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("UPDATE %v set indeks ='%s', skor = %d, mahasiswa_id = %d, mata_kuliah_id = %d, updated_at = NOW() where id = %s",
		table,
		nilai.Indeks,
		nilai.Skor,
		nilai.MahasiswaId,
		nilai.MataKuliahId,
		id,
	)

	_, err = db.ExecContext(ctx, queryText)
	if err != nil {
		return err
	}

	return nil
}

func Delete(ctx context.Context, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = %s", table, id)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ada")
	}

	if err != nil {
		fmt.Println(err.Error())
	}

	return nil
}
