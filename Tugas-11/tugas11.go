package main

import (
	"fmt"
	"math"
	"sort"
	"strconv"
	"sync"
)

// Soal 1 part 1
func printPhones(no int, phone string, wg *sync.WaitGroup) {
	fmt.Println(strconv.Itoa(no+1) + ". " + phone)
	wg.Done()
}

// Soal 2
func getMovies(movChan chan string, movies ...string) {
	for i, movie := range movies {
		movChan <- strconv.Itoa(i+1) + ". " + movie
	}

	close(movChan)
}

// Soal 3 part 1
func luasLingkaran(luasLingkaranChannel chan string, jariJari ...int) {
	for i, jariJari := range jariJari {
		luasLingkaranChannel <- "Luas Lingkaran ke-" + strconv.Itoa(i+1) + " = " + fmt.Sprintf("%f", math.Phi*float64(jariJari)*float64(jariJari))

	}
	close(luasLingkaranChannel)
}

func kelilingLingkaran(kelilingLingkaranChannel chan string, jariJari ...int) {
	for i, jariJari := range jariJari {
		kelilingLingkaranChannel <- "Keliling Lingkaran ke-" + strconv.Itoa(i+1) + " = " + fmt.Sprintf("%f", 2*math.Phi*float64(jariJari))

	}
	close(kelilingLingkaranChannel)
}

func volumeTabung(volumeTabungChannel chan string, tinggi int, jariJari ...int) {
	for i, jariJari := range jariJari {
		volumeTabungChannel <- "Volume Tabung ke-" + strconv.Itoa(i+1) + " = " + fmt.Sprintf("%f", math.Pi*(math.Pow(float64(jariJari), 2))*float64(tinggi))

	}
	close(volumeTabungChannel)
}

// Soal 4 part 1
func luasPersegiPanjang(luasPersegiPanjangChannel chan int, panjang int, lebar int) {
	luasPersegiPanjangChannel <- panjang * lebar
}

func kelilingPersegiPanjang(kelilingPersegiPanjangChannel chan int, panjang int, lebar int) {
	kelilingPersegiPanjangChannel <- 2 * (panjang + lebar)
}

func volumeBalok(volumeBalokChannel chan int, panjang int, lebar int, tinggi int) {
	volumeBalokChannel <- panjang * lebar * tinggi
}

func main() {
	var wg sync.WaitGroup

	var phones = []string{"Xiaomi", "Asus", "Iphone", "Samsung", "Oppo", "Realme", "Vivo"}

	// Soal 1 part 2
	sort.Strings(phones)

	for i := 0; i < len(phones); i++ {
		wg.Add(1)
		go printPhones(i, phones[i], &wg)
		wg.Wait()
	}

	var movies = []string{"Harry Potter", "LOTR", "SpiderMan", "Logan", "Avengers", "Insidious", "Toy Story"}

	moviesChannel := make(chan string)

	go getMovies(moviesChannel, movies...)

	for value := range moviesChannel {
		fmt.Println(value)
	}

	// Soal 3 part 2
	luasLingkaranChannel := make(chan string)
	kelilingLingkaranChannel := make(chan string)
	volumeTabungChannel := make(chan string)

	kumpulanJariJari := []int{8, 14, 20}
	tinggiTabung := 10

	go luasLingkaran(luasLingkaranChannel, kumpulanJariJari...)
	go kelilingLingkaran(kelilingLingkaranChannel, kumpulanJariJari...)
	go volumeTabung(volumeTabungChannel, tinggiTabung, kumpulanJariJari...)

	for hasil := range luasLingkaranChannel {
		fmt.Println(hasil)
	}

	for hasil := range kelilingLingkaranChannel {
		fmt.Println(hasil)
	}

	for hasil := range volumeTabungChannel {
		fmt.Println(hasil)
	}

	// Soal 4 part 2
	luasPersegiPanjangChannel := make(chan int)
	kelilingPersegiPanjangChannel := make(chan int)
	volumeBalokChannel := make(chan int)

	go luasPersegiPanjang(luasPersegiPanjangChannel, 5, 10)
	go kelilingPersegiPanjang(kelilingPersegiPanjangChannel, 5, 10)
	go volumeBalok(volumeBalokChannel, 5, 10, 2)

	for i := 0; i < 3; i++ {
		select {
		case luasPP := <-luasPersegiPanjangChannel:
			fmt.Println("Luas Persegi Panjang = " + strconv.Itoa(luasPP))
		case kelilingPP := <-kelilingPersegiPanjangChannel:
			fmt.Println("Keliling Persegi Panjang = " + strconv.Itoa(kelilingPP))
		case volBalok := <-volumeBalokChannel:
			fmt.Println("Volume Balok = " + strconv.Itoa(volBalok))
		}
	}

}
