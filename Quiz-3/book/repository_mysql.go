package book

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"quiz3/config"
	"quiz3/models"
	"time"
)

const (
	table          = "book"
	layoutDateTime = "2006-01-02 15:04:05"
)

func GetAll(ctx context.Context) ([]models.Book, error) {
	var kumpBuku []models.Book
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", table)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var buku models.Book
		var createdAt, updatedAt string
		if err = rowQuery.Scan(&buku.ID,
			&buku.Title,
			&buku.Description,
			&buku.ImageUrl,
			&buku.ReleaseYear,
			&buku.Price,
			&buku.TotalPage,
			&buku.Thickness,
			&createdAt,
			&updatedAt,
			&buku.CategoryId); err != nil {
			return nil, err
		}

		buku.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		buku.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		if err != nil {
			log.Fatal(err)
		}

		kumpBuku = append(kumpBuku, buku)
	}
	return kumpBuku, nil
}

func FilterGetAll(ctx context.Context, queryStr string) ([]models.Book, error) {
	var kumpBuku []models.Book
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v WHERE %v", table, queryStr)
	rowQuery, err := db.QueryContext(ctx, queryText)

	fmt.Println("QUERY =" + queryText)
	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var buku models.Book
		var createdAt, updatedAt string
		if err = rowQuery.Scan(&buku.ID,
			&buku.Title,
			&buku.Description,
			&buku.ImageUrl,
			&buku.ReleaseYear,
			&buku.Price,
			&buku.TotalPage,
			&buku.Thickness,
			&createdAt,
			&updatedAt,
			&buku.CategoryId); err != nil {
			return nil, err
		}

		buku.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		buku.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		if err != nil {
			log.Fatal(err)
		}

		kumpBuku = append(kumpBuku, buku)
	}
	return kumpBuku, nil
}

func GetByCategory(ctx context.Context, id string) ([]models.Book, error) {
	var kumpBuku []models.Book
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v WHERE category_id = %s Order By created_at DESC", table, id)
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var buku models.Book
		var createdAt, updatedAt string
		if err = rowQuery.Scan(&buku.ID,
			&buku.Title,
			&buku.Description,
			&buku.ImageUrl,
			&buku.ReleaseYear,
			&buku.Price,
			&buku.TotalPage,
			&buku.Thickness,
			&createdAt,
			&updatedAt,
			&buku.CategoryId); err != nil {
			return nil, err
		}

		buku.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		buku.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		if err != nil {
			log.Fatal(err)
		}

		kumpBuku = append(kumpBuku, buku)
	}
	return kumpBuku, nil
}

func FilterGetByCategory(ctx context.Context, id string, queryStr string) ([]models.Book, error) {
	var kumpBuku []models.Book
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v WHERE category_id = %s AND %v", table, id, queryStr)
	rowQuery, err := db.QueryContext(ctx, queryText)

	fmt.Println(queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var buku models.Book
		var createdAt, updatedAt string
		if err = rowQuery.Scan(&buku.ID,
			&buku.Title,
			&buku.Description,
			&buku.ImageUrl,
			&buku.ReleaseYear,
			&buku.Price,
			&buku.TotalPage,
			&buku.Thickness,
			&createdAt,
			&updatedAt,
			&buku.CategoryId); err != nil {
			return nil, err
		}

		buku.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		buku.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		if err != nil {
			log.Fatal(err)
		}

		kumpBuku = append(kumpBuku, buku)
	}
	return kumpBuku, nil
}

func Insert(ctx context.Context, buku models.Book) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("INSERT INTO %v (title, description, image_url, release_year, price, total_page, thickness, created_at, updated_at, category_id) values('%v', '%v', '%v', %v, '%v', %v, '%v', NOW(), NOW(), %v)", table,
		buku.Title,
		buku.Description,
		buku.ImageUrl,
		buku.ReleaseYear,
		buku.Price,
		buku.TotalPage,
		buku.Thickness,
		buku.CategoryId)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

func Update(ctx context.Context, buku models.Book, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("UPDATE %v set title='%s', description='%s', image_url='%s', release_year=%d, price='%s', total_page= %d, thickness= '%s', updated_at = NOW(), category_id=%d where id = %s",
		table,
		buku.Title,
		buku.Description,
		buku.ImageUrl,
		buku.ReleaseYear,
		buku.Price,
		buku.TotalPage,
		buku.Thickness,
		buku.CategoryId,
		id,
	)

	_, err = db.ExecContext(ctx, queryText)
	if err != nil {
		return err
	}

	return nil
}

func Delete(ctx context.Context, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = %s", table, id)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ada")
	}

	if err != nil {
		fmt.Println(err.Error())
	}

	return nil
}
