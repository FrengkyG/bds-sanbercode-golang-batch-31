package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"net/url"
	"quiz3/book"
	"quiz3/category"
	"quiz3/config"
	"quiz3/models"
	"quiz3/utils"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		if !ok {
			w.Write([]byte("Username atau Password tidak boleh kosong"))
			return
		}

		if username == "admin" && password == "password" {
			next.ServeHTTP(w, r)
			return
		} else if username == "editor" && password == "secret" {
			next.ServeHTTP(w, r)
			return
		} else if username == "trainer" && password == "rahasia" {
			next.ServeHTTP(w, r)
			return
		}

		w.Write([]byte("Username atau Password tidak sesuai"))
		return
	})
}

func main() {
	router := httprouter.New()

	db, e := config.MySQL()

	if e != nil {
		log.Fatal(e)
	}

	eb := db.Ping()
	if eb != nil {
		panic(eb.Error())
	}

	router.GET("/bangun-datar/segitiga-sama-sisi", segitiga)
	router.GET("/bangun-datar/persegi", persegi)
	router.GET("/bangun-datar/persegi-panjang", persegiPanjang)
	router.GET("/bangun-datar/lingkaran", lingkaran)
	router.GET("/bangun-datar/jajar-genjang", jajarGenjang)

	router.GET("/categories", GetCategories)
	router.GET("/categories/:id/books", GetBooksByCategories)
	router.POST("/categories", CreateCategories)
	router.PUT("/categories/:id", UpdateCategories)
	router.DELETE("/categories/:id", DeleteCategories)

	router.GET("/books", GetBooks)
	router.POST("/books", CreateBooks)
	router.PUT("/books/:id", UpdateBooks)
	router.DELETE("/books/:id", DeleteBooks)

	fmt.Println("Database Connected")
	fmt.Println("Server Running at Port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func segitiga(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	hitung := r.URL.Query().Get("hitung")
	getAlas := r.URL.Query().Get("alas")
	getTinggi := r.URL.Query().Get("tinggi")

	alas, _ := strconv.Atoi(getAlas)
	tinggi, _ := strconv.Atoi(getTinggi)

	luasSegitigaChannel := make(chan int)
	kelilingSegitigaChannel := make(chan int)

	if hitung == "luas" {
		go luasSegitiga(luasSegitigaChannel, alas, tinggi)
		res := map[string]string{
			"status":        "Success",
			"Luas Segitiga": strconv.Itoa(<-luasSegitigaChannel),
		}
		utils.ResponseJSON(w, res, http.StatusOK)
	} else if hitung == "keliling" {
		go kelilingSegitiga(kelilingSegitigaChannel, alas, tinggi)
		res := map[string]string{
			"status":            "Success",
			"Keliling Segitiga": strconv.Itoa(<-kelilingSegitigaChannel),
		}
		utils.ResponseJSON(w, res, http.StatusOK)
	} else {
		utils.ResponseJSON(w, "parameter hitung tidak sesuai, mohon isi dengan: luas atau keliling", http.StatusUnprocessableEntity)
	}
}

func luasSegitiga(luasSegitigaChannel chan int, alas, tinggi int) {
	luasSegitigaChannel <- (alas * tinggi) / 2
}

func kelilingSegitiga(kelilingSegitigaChannel chan int, alas, tinggi int) {
	kelilingSegitigaChannel <- alas + alas + alas
}

func persegi(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	hitung := r.URL.Query().Get("hitung")
	getSisi := r.URL.Query().Get("sisi")

	sisi, _ := strconv.Atoi(getSisi)

	luasPersegiChannel := make(chan int)
	kelilingPersegiChannel := make(chan int)

	if hitung == "luas" {
		go luasPersegi(luasPersegiChannel, sisi)
		res := map[string]string{
			"status":       "Success",
			"Luas Persegi": strconv.Itoa(<-luasPersegiChannel),
		}
		utils.ResponseJSON(w, res, http.StatusOK)
	} else if hitung == "keliling" {
		go kelilingPersegi(kelilingPersegiChannel, sisi)
		res := map[string]string{
			"status":            "Success",
			"Keliling Segitiga": strconv.Itoa(<-kelilingPersegiChannel),
		}
		utils.ResponseJSON(w, res, http.StatusOK)
	} else {
		utils.ResponseJSON(w, "parameter hitung tidak sesuai, mohon isi dengan: luas atau keliling", http.StatusUnprocessableEntity)
	}
}

func luasPersegi(luasPersegiChannel chan int, sisi int) {
	luasPersegiChannel <- sisi * sisi
}

func kelilingPersegi(kelilingPersegiChannel chan int, sisi int) {
	kelilingPersegiChannel <- 4 * sisi
}

func persegiPanjang(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	hitung := r.URL.Query().Get("hitung")
	getPanjang := r.URL.Query().Get("panjang")
	getLebar := r.URL.Query().Get("lebar")

	panjang, _ := strconv.Atoi(getPanjang)
	lebar, _ := strconv.Atoi(getLebar)

	luasPersegiPanjangChannel := make(chan int)
	kelilingPersegiPanjangChannel := make(chan int)

	if hitung == "luas" {
		go luasPersegiPanjang(luasPersegiPanjangChannel, panjang, lebar)
		res := map[string]string{
			"status":               "Success",
			"Luas Persegi Panjang": strconv.Itoa(<-luasPersegiPanjangChannel),
		}
		utils.ResponseJSON(w, res, http.StatusOK)
	} else if hitung == "keliling" {
		go kelilingPersegiPanjang(kelilingPersegiPanjangChannel, panjang, lebar)
		res := map[string]string{
			"status":                   "Success",
			"Keliling Persegi Panjang": strconv.Itoa(<-kelilingPersegiPanjangChannel),
		}
		utils.ResponseJSON(w, res, http.StatusOK)
	} else {
		utils.ResponseJSON(w, "parameter hitung tidak sesuai, mohon isi dengan: luas atau keliling", http.StatusUnprocessableEntity)
	}
}

func luasPersegiPanjang(luasPersegiPanjangChannel chan int, panjang, lebar int) {
	luasPersegiPanjangChannel <- panjang * lebar
}

func kelilingPersegiPanjang(kelilingPersegiPanjangChannel chan int, panjang, lebar int) {
	kelilingPersegiPanjangChannel <- 2 * (panjang + lebar)
}

func lingkaran(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	hitung := r.URL.Query().Get("hitung")
	getJariJari := r.URL.Query().Get("jariJari")

	jariJari, _ := strconv.Atoi(getJariJari)

	luasLingkaranChannel := make(chan float64)
	kelilingLingkaranChannel := make(chan float64)

	if hitung == "luas" {
		go luasLingkaran(luasLingkaranChannel, jariJari)
		res := map[string]string{
			"status":         "Success",
			"Luas Lingkaran": fmt.Sprintf("%f", <-luasLingkaranChannel),
		}
		utils.ResponseJSON(w, res, http.StatusOK)
	} else if hitung == "keliling" {
		go kelilingLingkaran(kelilingLingkaranChannel, jariJari)
		res := map[string]string{
			"status":             "Success",
			"Keliling Lingkaran": fmt.Sprintf("%f", <-kelilingLingkaranChannel),
		}
		utils.ResponseJSON(w, res, http.StatusOK)
	} else {
		utils.ResponseJSON(w, "parameter hitung tidak sesuai, mohon isi dengan: luas atau keliling", http.StatusUnprocessableEntity)
	}
}

func luasLingkaran(luasLingkaranChannel chan float64, jariJari int) {
	luasLingkaranChannel <- math.Pi * float64(jariJari) * float64(jariJari)
}

func kelilingLingkaran(kelilingLingkaranChannel chan float64, jariJari int) {
	kelilingLingkaranChannel <- 2 * math.Pi * float64(jariJari)
}

func jajarGenjang(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	hitung := r.URL.Query().Get("hitung")
	getAlas := r.URL.Query().Get("alas")
	getTinggi := r.URL.Query().Get("tinggi")
	getSisi := r.URL.Query().Get("sisi")

	alas, _ := strconv.Atoi(getAlas)
	tinggi, _ := strconv.Atoi(getTinggi)
	sisi, _ := strconv.Atoi(getSisi)

	luasJajarGenjangChannel := make(chan int)
	kelilingJajarGenjangChannel := make(chan int)

	if hitung == "luas" {
		go luasJajarGenjang(luasJajarGenjangChannel, alas, tinggi)
		res := map[string]string{
			"status":             "Success",
			"Luas Jajar Genjang": strconv.Itoa(<-luasJajarGenjangChannel),
		}
		utils.ResponseJSON(w, res, http.StatusOK)
	} else if hitung == "keliling" {
		go kelilingJajarGenjang(kelilingJajarGenjangChannel, alas, sisi)
		res := map[string]string{
			"status":                 "Success",
			"Keliling Jajar Genjang": strconv.Itoa(<-kelilingJajarGenjangChannel),
		}
		utils.ResponseJSON(w, res, http.StatusOK)
	} else {
		utils.ResponseJSON(w, "parameter hitung tidak sesuai, mohon isi dengan: luas atau keliling", http.StatusUnprocessableEntity)
	}
}

func luasJajarGenjang(luasJajarGenjangChannel chan int, alas, tinggi int) {
	luasJajarGenjangChannel <- alas * tinggi
}

func kelilingJajarGenjang(kelilingJajarGenjangChannel chan int, alas, sisi int) {
	kelilingJajarGenjangChannel <- (2 * alas) + (2 * sisi)
}

func GetCategories(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	category, err := category.GetAll(ctx)
	if err != nil {
		fmt.Println(err)
	}

	utils.ResponseJSON(w, category, http.StatusOK)
}

func CreateCategories(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var cat models.Category
	if err := json.NewDecoder(r.Body).Decode(&cat); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	if err := category.Insert(ctx, cat); err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)

}

func UpdateCategories(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var cat models.Category

	if err := json.NewDecoder(r.Body).Decode(&cat); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	var idCat = params.ByName("id")

	if err := category.Update(ctx, cat, idCat); err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Berhasil Add Category",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

func DeleteCategories(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var idCat = params.ByName("id")
	if err := category.Delete(ctx, idCat); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusOK)
}

func GetBooks(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var queryString string

	if len(r.URL.Query()) == 0 {
		buku, err := book.GetAll(ctx)
		if err != nil {
			fmt.Println(err)
		}
		utils.ResponseJSON(w, buku, http.StatusOK)
	} else {
		flag := 1
		lenQuery := len(r.URL.Query())
		title := r.URL.Query().Get("title")
		minYear := r.URL.Query().Get("minYear")
		maxYear := r.URL.Query().Get("maxYear")
		minPage := r.URL.Query().Get("minPage")
		maxPage := r.URL.Query().Get("maxPage")
		sortByTitle := r.URL.Query().Get("sortByTitle")

		if title != "" {
			queryString += "TITLE LIKE " + "\"" + title + "\""
			flag++
			if flag < lenQuery {
				queryString += " AND "
			}
		}

		if minYear != "" {
			queryString += "RELEASE_YEAR > " + minYear
			flag++
			if flag < lenQuery {
				queryString += " AND "
			}
		}

		if maxYear != "" {
			queryString += "RELEASE_YEAR < " + maxYear
			flag++
			if flag < lenQuery {
				queryString += " AND "
			}
		}

		if minPage != "" {
			queryString += "TOTAL_PAGE > " + minPage
			flag++
			if flag < lenQuery {
				queryString += " AND "
			}
		}

		if maxPage != "" {
			queryString += "TOTAL_PAGE < " + maxPage
			flag++
		}

		if sortByTitle != "" {
			queryString += " ORDER BY TITLE " + sortByTitle
		}

		buku, err := book.FilterGetAll(ctx, queryString)
		if err != nil {
			fmt.Println(err)
		}
		utils.ResponseJSON(w, buku, http.StatusOK)
	}

}

func GetBooksByCategories(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var queryString string
	if len(r.URL.Query()) == 0 {
		buku, err := book.GetByCategory(ctx, params.ByName("id"))
		if err != nil {
			fmt.Println(err)
		}
		utils.ResponseJSON(w, buku, http.StatusOK)
	} else {
		flag := 1
		lenQuery := len(r.URL.Query())
		title := r.URL.Query().Get("title")
		minYear := r.URL.Query().Get("minYear")
		maxYear := r.URL.Query().Get("maxYear")
		minPage := r.URL.Query().Get("minPage")
		maxPage := r.URL.Query().Get("maxPage")
		sortByTitle := r.URL.Query().Get("sortByTitle")

		if title != "" {
			queryString += "TITLE LIKE " + "\"" + title + "\""
			flag++
			if flag < lenQuery {
				queryString += " AND "
			}
		}

		if minYear != "" {
			queryString += "RELEASE_YEAR > " + minYear
			flag++
			if flag < lenQuery {
				queryString += " AND "
			}
		}

		if maxYear != "" {
			queryString += "RELEASE_YEAR < " + maxYear
			flag++
			if flag < lenQuery {
				queryString += " AND "
			}
		}

		if minPage != "" {
			queryString += "TOTAL_PAGE > " + minPage
			flag++
			if flag < lenQuery {
				queryString += " AND "
			}
		}

		if maxPage != "" {
			queryString += "TOTAL_PAGE < " + maxPage
			flag++
		}

		if sortByTitle != "" {
			queryString += " ORDER BY TITLE " + sortByTitle
		}

		buku, err := book.FilterGetByCategory(ctx, params.ByName("id"), queryString)
		if err != nil {
			fmt.Println(err)
		}
		utils.ResponseJSON(w, buku, http.StatusOK)
	}
}

func CreateBooks(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var buku models.Book

	if err := json.NewDecoder(r.Body).Decode(&buku); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	} else {
		// Validation
		_, errMsg := url.ParseRequestURI(buku.ImageUrl)
		if errMsg != nil {
			if buku.ReleaseYear < 1980 || buku.ReleaseYear > 2021 {
				http.Error(w, ("Release Year Harus di antara 1980-2021\n" + errMsg.Error()), http.StatusUnprocessableEntity)
				return
			} else {
				http.Error(w, errMsg.Error(), http.StatusUnprocessableEntity)
				return
			}
		} else {
			if buku.ReleaseYear < 1980 || buku.ReleaseYear > 2021 {
				http.Error(w, "Release Year Harus di antara 1980-2021", http.StatusUnprocessableEntity)
				return
			}
		}

		// Convertion
		if buku.TotalPage <= 100 {
			buku.Thickness = "tipis"
		} else if buku.TotalPage > 101 && buku.TotalPage <= 200 {
			buku.Thickness = "sedang"
		} else {
			buku.Thickness = "tebal"
		}
	}

	if err := book.Insert(ctx, buku); err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusCreated)
}

func UpdateBooks(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var buku models.Book

	if err := json.NewDecoder(r.Body).Decode(&buku); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	} // Validation
	_, errMsg := url.ParseRequestURI(buku.ImageUrl)
	if errMsg != nil {
		if buku.ReleaseYear < 1980 || buku.ReleaseYear > 2021 {
			http.Error(w, ("Release Year Harus di antara 1980-2021 &" + errMsg.Error()), http.StatusUnprocessableEntity)
			return
		} else {
			http.Error(w, errMsg.Error(), http.StatusUnprocessableEntity)
			return
		}
	} else {
		if buku.ReleaseYear < 1980 || buku.ReleaseYear > 2021 {
			http.Error(w, "Release Year Harus di antara 1980-2021 &", http.StatusUnprocessableEntity)
			return
		}
	}

	// Convertion
	if buku.TotalPage <= 100 {
		buku.Thickness = "tipis"
	} else if buku.TotalPage > 101 && buku.TotalPage <= 200 {
		buku.Thickness = "sedang"
	} else {
		buku.Thickness = "tebal"
	}

	var idBuku = params.ByName("id")

	if err := book.Update(ctx, buku, idBuku); err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

func DeleteBooks(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var idBuku = params.ByName("id")
	if err := book.Delete(ctx, idBuku); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusOK)
}
