package main

import (
	"fmt"
	"math"
	"strconv"
)

type segitigaSamaSisi struct {
	alas, tinggi int
}

type persegiPanjang struct {
	panjang, lebar int
}

type tabung struct {
	jariJari, tinggi float64
}

type balok struct {
	panjang, lebar, tinggi int
}

type hitungBangunDatar interface {
	luas() int
	keliling() int
}

type hitungBangunRuang interface {
	volume() float64
	luasPermukaan() float64
}

// Soal 1 part 1
func (s segitigaSamaSisi) luas() int {
	return (s.alas * s.tinggi) / 2
}

func (s segitigaSamaSisi) keliling() int {
	return s.alas + s.alas + s.alas
}

func (pp persegiPanjang) luas() int {
	return pp.panjang * pp.lebar
}

func (pp persegiPanjang) keliling() int {
	return (2 * pp.panjang) + (2 * pp.lebar)
}

func (tb tabung) volume() float64 {
	return math.Pi * (math.Pow(tb.jariJari, 2) * tb.tinggi)
}

func (tb tabung) luasPermukaan() float64 {
	return 2 * math.Pi * tb.jariJari * (tb.tinggi + tb.jariJari)
}

func (b balok) volume() float64 {
	return float64(b.panjang) * float64(b.lebar) * float64(b.tinggi)
}

func (b balok) luasPermukaan() float64 {
	return 2 * ((float64(b.panjang) * float64(b.lebar)) + (float64(b.panjang) * float64(b.tinggi)) + (float64(b.lebar) * float64(b.tinggi)))
}

type phone struct {
	name, brand string
	year        int
	colors      []string
}

//  Soal 2 part 1
type phoneDetail interface {
	getPhoneDetail() string
}

func (p phone) getPhoneDetail() string {
	result := "name : " + p.name + "\n" +
		"brand : " + p.brand + "\n" +
		"year : " + strconv.Itoa(p.year) + "\n" +
		"colors: "

	for _, warna := range p.colors {
		result += warna + ", "
	}
	return result
}

// Soal 3
func luasPersegi(sisi int, f bool) interface{} {
	if sisi != 0 && f {
		return "Luas persegi dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(sisi*sisi) + " cm"
	} else if sisi != 0 && !f {
		return sisi * sisi
	} else if sisi == 0 && f {
		return "Maaf anda belum menginput sisi dari persegi"
	} else {
		return nil
	}
}

func main() {
	// Soal 1 part 2
	var bangunDatar hitungBangunDatar
	var bangunRuang hitungBangunRuang

	bangunDatar = segitigaSamaSisi{alas: 10, tinggi: 5}
	fmt.Println("Luas segitiga sama sisi = ", bangunDatar.luas())
	fmt.Println("Keliling segitiga sama sisi = ", bangunDatar.keliling())

	bangunDatar = persegiPanjang{panjang: 20, lebar: 10}
	fmt.Println("Luas persegi panjang = ", bangunDatar.luas())
	fmt.Println("Keliling persegi panjang = ", bangunDatar.keliling())

	bangunRuang = tabung{jariJari: 10, tinggi: 5}
	fmt.Println("Volume Tabung = ", bangunRuang.volume())
	fmt.Println("Luas Permukaan Tabung = ", bangunRuang.luasPermukaan())

	bangunRuang = balok{panjang: 5, lebar: 7, tinggi: 9}
	fmt.Println("Volume balok = ", bangunRuang.volume())
	fmt.Println("Luas Permukaan balok = ", bangunRuang.luasPermukaan())

	// Soal 2 part 2
	var telepon phoneDetail
	telepon = phone{name: "Samsung Galaxy Note 20", brand: "Samsung Galaxy Note 20", year: 2020, colors: []string{"Mystic Bronze", "Mystic White", "Mysthic Black"}}
	fmt.Println(telepon.getPhoneDetail())

	fmt.Println(luasPersegi(4, true))
	fmt.Println(luasPersegi(8, false))
	fmt.Println(luasPersegi(0, true))
	fmt.Println(luasPersegi(0, false))

	var prefix interface{} = "hasil penjumlahan dari "

	var kumpulanAngkaPertama interface{} = []int{6, 8}

	var kumpulanAngkaKedua interface{} = []int{12, 14}

	// Soal 4
	var hasilKalimat = prefix.(string)
	var hasilHitung int
	for i := 0; i < len(kumpulanAngkaPertama.([]int)); i++ {
		hasilHitung += kumpulanAngkaPertama.([]int)[i]
		hasilKalimat += strconv.Itoa(kumpulanAngkaPertama.([]int)[i])
		hasilKalimat += "+"
	}

	for j := 0; j < len(kumpulanAngkaKedua.([]int)); j++ {
		hasilHitung += kumpulanAngkaKedua.([]int)[j]
		hasilKalimat += strconv.Itoa(kumpulanAngkaKedua.([]int)[j])
		if j+1 < len(kumpulanAngkaKedua.([]int)) {
			hasilKalimat += "+"
		} else {
			hasilKalimat += "="
		}
	}
	fmt.Println(hasilKalimat + strconv.Itoa(hasilHitung))

}
