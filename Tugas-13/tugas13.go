package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"tugas13/config"
	"tugas13/mahasiswa"
	"tugas13/models"
	"tugas13/utils"

	"github.com/julienschmidt/httprouter"
)

func GetMahasiswa(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	mahasiswa, err := mahasiswa.GetAll(ctx)
	if err != nil {
		fmt.Println(err)
	}

	utils.ResponseJSON(w, mahasiswa, http.StatusOK)
}

func PostMahasiswa(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var mhs models.Mahasiswa

	if err := json.NewDecoder(r.Body).Decode(&mhs); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	} else {
		if mhs.Nilai > 100 {
			http.Error(w, "Nilai Maksimum 100", http.StatusUnprocessableEntity)
			return
		} else if mhs.Nilai >= 80 && mhs.Nilai <= 100 {
			mhs.IndeksNilai = "A"
		} else if mhs.Nilai >= 70 && mhs.Nilai < 80 {
			mhs.IndeksNilai = "B"
		} else if mhs.Nilai >= 60 && mhs.Nilai < 70 {
			mhs.IndeksNilai = "C"
		} else if mhs.Nilai >= 50 && mhs.Nilai < 60 {
			mhs.IndeksNilai = "D"
		} else {
			mhs.IndeksNilai = "E"
		}
	}
	if err := mahasiswa.Insert(ctx, mhs); err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusCreated)
}

func UpdateMahasiswa(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var mhs models.Mahasiswa

	if err := json.NewDecoder(r.Body).Decode(&mhs); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	} else {
		if mhs.Nilai > 100 {
			http.Error(w, "Nilai Maksimum 100", http.StatusUnprocessableEntity)
			return
		} else if mhs.Nilai >= 80 && mhs.Nilai <= 100 {
			mhs.IndeksNilai = "A"
		} else if mhs.Nilai >= 70 && mhs.Nilai < 80 {
			mhs.IndeksNilai = "B"
		} else if mhs.Nilai >= 60 && mhs.Nilai < 70 {
			mhs.IndeksNilai = "C"
		} else if mhs.Nilai >= 50 && mhs.Nilai < 60 {
			mhs.IndeksNilai = "D"
		} else {
			mhs.IndeksNilai = "E"
		}
	}

	var idMahasiswa = params.ByName("id")

	if err := mahasiswa.Update(ctx, mhs, idMahasiswa); err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

func DeleteMahasiswa(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var idMahasiswa = params.ByName("id")
	if err := mahasiswa.Delete(ctx, idMahasiswa); err != nil {
		kesalahan := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusOK)
}

func main() {
	router := httprouter.New()

	db, e := config.MySQL()

	if e != nil {
		log.Fatal(e)
	}

	eb := db.Ping()
	if eb != nil {
		panic(eb.Error())
	}

	router.POST("/mahasiswa/create", PostMahasiswa)
	router.GET("/mahasiswa", GetMahasiswa)
	router.PUT("/mahasiswa/:id/update", UpdateMahasiswa)
	router.DELETE("/mahasiswa/:id/delete", DeleteMahasiswa)

	fmt.Println("MySQL Connected")
	fmt.Println("Server Running at Port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
