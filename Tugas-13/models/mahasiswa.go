package models

import "time"

type (
	Mahasiswa struct {
		Nama        string `json:"nama"`
		MataKuliah  string `json:"mataKuliah"`
		IndeksNilai string
		Nilai       uint
		ID          uint
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
	}
)
