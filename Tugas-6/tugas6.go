package main

import (
	"fmt"
)

// Soal 1
func updateLingkaran(jariJari float64, luasLingkaran *float64, kelilingLingkaran *float64) {
	var phi float64 = 3.14
	*luasLingkaran = phi * jariJari * jariJari
	*kelilingLingkaran = 2 * phi * jariJari
}

// Soal 2
func introduce(sentence *string, nama string, gender string, pekerjaan string, umur string) {
	if gender == "laki-laki" {
		*sentence = "Pak " + nama + " adalah seorang " + pekerjaan + " yang berusia " + umur + " tahun"
	} else {
		*sentence = "Bu " + nama + " adalah seorang " + pekerjaan + " yang berusia " + umur + " tahun"
	}
}

// Soal 3 part 1
func tambahBuah(buah *[]string, namaBuah string) {
	*buah = append(*buah, namaBuah)
}

// Soal4 part 1
func tambahDataFilm(nama string, durasi string, genre string, tahun string, dataFilm *[]map[string]string) {
	film := make(map[string]string)
	film["genre"] = genre
	film["jam"] = durasi
	film["tahun"] = tahun
	film["title"] = nama
	*dataFilm = append(*dataFilm, film)
}

func main() {

	var luasLingkaran float64
	var kelilingLingkaran float64
	updateLingkaran(4, &luasLingkaran, &kelilingLingkaran)

	var sentence string
	introduce(&sentence, "John", "laki-laki", "penulis", "30")

	fmt.Println(sentence) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"
	introduce(&sentence, "Sarah", "perempuan", "model", "28")

	fmt.Println(sentence) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	var buah = []string{}
	tambahBuah(&buah, "Jeruk")
	tambahBuah(&buah, "Semangka")
	tambahBuah(&buah, "Mangga")
	tambahBuah(&buah, "Strawberry")
	tambahBuah(&buah, "Durian")
	tambahBuah(&buah, "Manggis")
	tambahBuah(&buah, "Alpukat")

	//Soal 3 part 2
	for i := 0; i < len(buah); i++ {
		fmt.Print(i + 1)
		fmt.Println(". ", buah[i])
	}

	var dataFilm = []map[string]string{}

	tambahDataFilm("LOTR", "2 jam", "action", "1999", &dataFilm)
	tambahDataFilm("avenger", "2 jam", "action", "2019", &dataFilm)
	tambahDataFilm("spiderman", "2 jam", "action", "2004", &dataFilm)
	tambahDataFilm("juon", "2 jam", "horror", "2004", &dataFilm)

	// Soal 4 part 2
	for i := 0; i < len(dataFilm); i++ {
		fmt.Print(i + 1)
		fmt.Println(". title : " + dataFilm[i]["title"])
		fmt.Println("   duration : " + dataFilm[i]["jam"])
		fmt.Println("   genre : " + dataFilm[i]["genre"])
		fmt.Println("   year : " + dataFilm[i]["tahun"])
	}
}
