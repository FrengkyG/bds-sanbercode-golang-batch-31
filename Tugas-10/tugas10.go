package main

import (
	"flag"
	"fmt"
	"math"
	"sort"
	"strconv"
	"time"
)

// Soal 1 Part 1
func say(kalimat string, tahun int) {
	fmt.Println(kalimat, tahun)

	// Soal 2  Part 2
	errorMsg := recover()
	if errorMsg != nil {
		fmt.Println("Terjadi error : ", errorMsg)
	} else {
		fmt.Println("")
	}

}

// Soal 2  Part 1
func kelilingSegitigaSamaSisi(sisi int, f bool) interface{} {
	if sisi != 0 && f {
		return "keliling segitiga sama sisinya dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(3*sisi) + " cm"
	} else if sisi != 0 && !f {
		return 3 * sisi
	} else if sisi == 0 && f {
		return "Maaf anda belum menginput sisi dari segitiga sama sisi"
	} else {
		panic("Maaf anda belum menginput sisi dari segitiga sama sisi")
	}

}

// Soal 3
func tambahAngka(number int, angka *int) {
	*angka += number
}

func cetakAngka(angka *int) {
	fmt.Println("Angka sekarang adalah ", strconv.Itoa(*angka))
}

// Soal 4 part 1
func addPhone(phones *[]string, newPhone string) {
	*phones = append(*phones, newPhone)
}

// Soal 5 part 1
func luasLingkaran(jariJari int) float64 {
	return math.Round(math.Pi * float64(jariJari) * float64(jariJari))
}

func kelilingLingkaran(jariJari int) float64 {
	return math.Round(2 * math.Pi * float64(jariJari))
}

// Soal 6 part 1
func luasPersegiPanjang(panjang, lebar int) int {
	return panjang * lebar
}

func kelilingPersegiPanjang(panjang, lebar int) int {
	return ((2 * panjang) + (2 * lebar))
}

func main() {
	angka := 1
	defer cetakAngka(&angka)
	tambahAngka(7, &angka)
	tambahAngka(6, &angka)
	tambahAngka(-1, &angka)
	tambahAngka(9, &angka)

	var phones = []string{}

	// Soal 4 part 2
	addPhone(&phones, "Xiaomi")
	addPhone(&phones, "Asus")
	addPhone(&phones, "IPhone")
	addPhone(&phones, "Samsung")
	addPhone(&phones, "Oppo")
	addPhone(&phones, "Realme")
	addPhone(&phones, "Vivo")
	sort.Strings(phones)

	for i, item := range phones {
		fmt.Println(strconv.Itoa(i+1) + ". " + item)
		time.Sleep(time.Second * 1)
	}

	// Soal 5 part 2
	fmt.Println("Luas lingkaran dengan jari-jari 7 =", luasLingkaran(7))
	fmt.Println("Keliling lingkaran dengan jari-jari 7 =", kelilingLingkaran(7))
	fmt.Println("Luas lingkaran dengan jari-jari 10 =", luasLingkaran(10))
	fmt.Println("Keliling lingkaran dengan jari-jari 10 =", kelilingLingkaran(10))
	fmt.Println("Luas lingkaran dengan jari-jari 15 =", luasLingkaran(15))
	fmt.Println("Keliling lingkaran dengan jari-jari 15 =", kelilingLingkaran(15))

	// Soal 6 part 2
	var panjang = flag.Int("panjang", 10, "Masukan panjang dari persegi panjang")
	var lebar = flag.Int("lebar", 5, "Masukan lebar dari persegi panjang")

	flag.Parse()
	fmt.Println("Luas Persegi Panjang=", luasPersegiPanjang(*panjang, *lebar))
	fmt.Println("Keliling Persegi Panjang=", kelilingPersegiPanjang(*panjang, *lebar))

	// Soal 1 Part 2
	defer say("Golang Backend Development", 2021)

	fmt.Println(kelilingSegitigaSamaSisi(4, true))
	fmt.Println(kelilingSegitigaSamaSisi(8, false))
	fmt.Println(kelilingSegitigaSamaSisi(0, true))
	fmt.Println(kelilingSegitigaSamaSisi(0, false))

}
