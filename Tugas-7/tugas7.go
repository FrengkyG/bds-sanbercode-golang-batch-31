package main

import (
	"fmt"
)

// Soal 1 part 1
type buah struct {
	nama, warna string
	adaBijinya  bool
	harga       int
}

type segitiga struct {
	alas, tinggi int
}

type persegi struct {
	sisi int
}

type persegiPanjang struct {
	panjang, lebar int
}

// Soal 2 part 1
func (s segitiga) luasSegitiga() int {
	return s.alas * s.tinggi / 2
}

func (p persegi) luasPersegi() int {
	return p.sisi * p.sisi
}
func (pp persegiPanjang) luasPersegiPanjang() int {
	return pp.panjang * pp.lebar
}

type phone struct {
	name, brand string
	year        int
	colors      []string
}

// Soal 3
func (ph *phone) addColors(color string) {
	ph.colors = append(ph.colors, color)
}

// Soal 4 part 1
type movie struct {
	title, genre   string
	duration, year int
}

func tambahDataFilm(title string, duration int, genre string, year int, dataFilm *[]movie) {
	dataMovie := movie{
		title:    title,
		duration: duration,
		genre:    genre,
		year:     year,
	}
	*dataFilm = append(*dataFilm, dataMovie)
}

func main() {
	// Soal 1 part 2
	nanas := buah{
		nama:       "Nanas",
		warna:      "Kuning",
		adaBijinya: false,
		harga:      9000,
	}
	jeruk := buah{
		nama:       "Jeruk",
		warna:      "Oranye",
		adaBijinya: true,
		harga:      8000,
	}
	semangka := buah{
		nama:       "Semangka",
		warna:      "Hijau & Merah",
		adaBijinya: true,
		harga:      10000,
	}
	pisang := buah{
		nama:       "Pisang",
		warna:      "Kuning",
		adaBijinya: false,
		harga:      5000,
	}

	fmt.Println(nanas)
	fmt.Println(jeruk)
	fmt.Println(semangka)
	fmt.Println(pisang)

	// Soal 2 part 2
	var segitiga = segitiga{
		alas:   4,
		tinggi: 10,
	}

	var persegi = persegi{}
	persegi.sisi = 5

	var persegiPanjang = persegiPanjang{
		panjang: 2,
		lebar:   3,
	}

	fmt.Println("Luas Segitiga = ", segitiga.luasSegitiga())
	fmt.Println("Luas Persegi = ", persegi.luasPersegi())
	fmt.Println("Luas Persegi Panjang = ", persegiPanjang.luasPersegiPanjang())

	// Test Jawaban Soal 3
	xiaomi := phone{
		name:  "Mi 1000",
		brand: "Xiaomi",
		year:  2030,
	}
	xiaomi.addColors("black")
	xiaomi.addColors("blue")
	xiaomi.addColors("white")
	fmt.Println(xiaomi)

	var dataFilm = []movie{}

	tambahDataFilm("LOTR", 120, "action", 1999, &dataFilm)
	tambahDataFilm("avenger", 120, "action", 2019, &dataFilm)
	tambahDataFilm("spiderman", 120, "action", 2004, &dataFilm)
	tambahDataFilm("juon", 120, "horror", 2004, &dataFilm)

	// Soal 4 part 2
	for i := 0; i < len(dataFilm); i++ {
		fmt.Print(i + 1)
		fmt.Println(". title : " + dataFilm[i].title)
		fmt.Println("   duration :", (dataFilm[i].duration)/60, "jam")
		fmt.Println("   genre : " + dataFilm[i].genre)
		fmt.Println("   year : ", dataFilm[i].year)
	}
}
