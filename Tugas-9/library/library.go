package library

import (
	"math"
	"strconv"
)

type SegitigaSamaSisi struct {
	Alas, Tinggi int
}

type PersegiPanjang struct {
	Panjang, Lebar int
}

type Tabung struct {
	Jarijari, Tinggi float64
}

type Balok struct {
	Panjang, Lebar, Tinggi int
}

type HitungBangunDatar interface {
	Luas() int
	Keliling() int
}

type HitungBangunRuang interface {
	Volume() float64
	LuasPermukaan() float64
}

// Soal 1 part 1
func (s SegitigaSamaSisi) Luas() int {
	return (s.Alas * s.Tinggi) / 2
}

func (s SegitigaSamaSisi) Keliling() int {
	return s.Alas + s.Alas + s.Alas
}

func (pp PersegiPanjang) Luas() int {
	return pp.Panjang * pp.Lebar
}

func (pp PersegiPanjang) Keliling() int {
	return (2 * pp.Panjang) + (2 * pp.Lebar)
}

func (tb Tabung) Volume() float64 {
	return math.Pi * (math.Pow(tb.Jarijari, 2) * tb.Tinggi)
}

func (tb Tabung) LuasPermukaan() float64 {
	return 2 * math.Pi * tb.Jarijari * (tb.Tinggi + tb.Jarijari)
}

func (b Balok) Volume() float64 {
	return float64(b.Panjang) * float64(b.Lebar) * float64(b.Tinggi)
}

func (b Balok) LuasPermukaan() float64 {
	return 2 * ((float64(b.Panjang) * float64(b.Lebar)) + (float64(b.Panjang) * float64(b.Tinggi)) + (float64(b.Lebar) * float64(b.Tinggi)))
}

type Phone struct {
	Name, Brand string
	Year        int
	Colors      []string
}

//  Soal 2 part 1
type PhoneDetail interface {
	GetPhoneDetail() string
}

func (p Phone) GetPhoneDetail() string {
	result := "name : " + p.Name + "\n" +
		"brand : " + p.Brand + "\n" +
		"year : " + strconv.Itoa(p.Year) + "\n" +
		"colors: "

	for _, warna := range p.Colors {
		result += warna + ", "
	}
	return result
}

// Soal 3
func LuasPersegi(sisi int, f bool) interface{} {
	if sisi != 0 && f {
		return "Luas persegi dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(sisi*sisi) + " cm"
	} else if sisi != 0 && !f {
		return sisi * sisi
	} else if sisi == 0 && f {
		return "Maaf anda belum menginput sisi dari persegi"
	} else {
		return nil
	}
}
