package main

import (
	"fmt"
	"strconv"
	lb "tugas-9/library"
)

func main() {
	// Soal 1 part 2
	var bangunDatar lb.HitungBangunDatar
	var bangunRuang lb.HitungBangunRuang

	bangunDatar = lb.SegitigaSamaSisi{Alas: 10, Tinggi: 5}
	fmt.Println("Luas segitiga sama sisi = ", bangunDatar.Luas())
	fmt.Println("Keliling segitiga sama sisi = ", bangunDatar.Keliling())

	bangunDatar = lb.PersegiPanjang{Panjang: 20, Lebar: 10}
	fmt.Println("Luas persegi panjang = ", bangunDatar.Luas())
	fmt.Println("Keliling persegi panjang = ", bangunDatar.Keliling())

	bangunRuang = lb.Tabung{Jarijari: 10, Tinggi: 5}
	fmt.Println("Volume Tabung = ", bangunRuang.Volume())
	fmt.Println("Luas Permukaan Tabung = ", bangunRuang.LuasPermukaan())

	bangunRuang = lb.Balok{Panjang: 5, Lebar: 7, Tinggi: 9}
	fmt.Println("Volume balok = ", bangunRuang.Volume())
	fmt.Println("Luas Permukaan balok = ", bangunRuang.LuasPermukaan())

	// Soal 2 part 2
	var telepon lb.PhoneDetail
	telepon = lb.Phone{Name: "Samsung Galaxy Note 20", Brand: "Samsung Galaxy Note 20", Year: 2020, Colors: []string{"Mystic Bronze", "Mystic White", "Mysthic Black"}}
	fmt.Println(telepon.GetPhoneDetail())

	fmt.Println(lb.LuasPersegi(4, true))
	fmt.Println(lb.LuasPersegi(8, false))
	fmt.Println(lb.LuasPersegi(0, true))
	fmt.Println(lb.LuasPersegi(0, false))

	var prefix interface{} = "hasil penjumlahan dari "

	var kumpulanAngkaPertama interface{} = []int{6, 8}

	var kumpulanAngkaKedua interface{} = []int{12, 14}

	// Soal 4
	var hasilKalimat = prefix.(string)
	var hasilHitung int
	for i := 0; i < len(kumpulanAngkaPertama.([]int)); i++ {
		hasilHitung += kumpulanAngkaPertama.([]int)[i]
		hasilKalimat += strconv.Itoa(kumpulanAngkaPertama.([]int)[i])
		hasilKalimat += "+"
	}

	for j := 0; j < len(kumpulanAngkaKedua.([]int)); j++ {
		hasilHitung += kumpulanAngkaKedua.([]int)[j]
		hasilKalimat += strconv.Itoa(kumpulanAngkaKedua.([]int)[j])
		if j+1 < len(kumpulanAngkaKedua.([]int)) {
			hasilKalimat += "+"
		} else {
			hasilKalimat += "="
		}
	}
	fmt.Println(hasilKalimat + strconv.Itoa(hasilHitung))

}
