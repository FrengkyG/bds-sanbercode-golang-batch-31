package main

import (
	"fmt"
)

func main() {
	// Soal 1
	for i := 1; i <= 20; i++ {
		if i%3 == 0 && i%2 == 1 {
			fmt.Println(i, " - I Love Coding")
		} else if i%2 == 0 {
			fmt.Println(i, " - Berkualitas")
		} else {
			fmt.Println(i, " - Santai")
		}
	}

	// Soal 2
	for i := 1; i <= 7; i++ {
		for j := 0; j < i; j++ {
			fmt.Print("#")
		}
		fmt.Println()
	}

	// Soal 3
	var kalimat = [...]string{"aku", "dan", "saya", "sangat", "senang", "belajar", "golang"}

	var hasilKalimat = kalimat[2:7]

	fmt.Println(hasilKalimat)
	// Soal 4
	var sayuran = []string{}

	sayuran = append(sayuran, "Bayam")
	sayuran = append(sayuran, "Buncis")
	sayuran = append(sayuran, "Kangkung")
	sayuran = append(sayuran, "Kubis")
	sayuran = append(sayuran, "Seledri")
	sayuran = append(sayuran, "Tauge")
	sayuran = append(sayuran, "Timun")

	for i := 0; i < len(sayuran); i++ {
		fmt.Print(i + 1)
		fmt.Println(". ", sayuran[i])
	}

	// Soal 5
	var satuan = map[string]int{
		"panjang": 7,
		"lebar":   4,
		"tinggi":  6,
	}

	satuan["volumeBalok"] = satuan["panjang"] * satuan["lebar"] * satuan["tinggi"]

	for key, value := range satuan {
		fmt.Println(key, "=", value)
	}

}
