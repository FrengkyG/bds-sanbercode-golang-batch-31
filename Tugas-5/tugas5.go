package main

import (
	"fmt"
)

// Soal 1
func luasPersegiPanjang(panjang int, lebar int) int {
	result := panjang * lebar
	return result
}

func kelilingPersegiPanjang(panjang int, lebar int) int {
	result := 2 * (panjang + lebar)
	return result
}

func volumeBalok(panjang int, lebar int, tinggi int) int {
	result := panjang * lebar * tinggi
	return result
}

// Soal 2
func introduce(nama, gender, pekerjaan, umur string) (perkenalan string) {
	if gender == "laki-laki" {
		perkenalan = "Pak " + nama + " adalah seorang " + pekerjaan + " yang berusia " + umur + " tahun"
	} else {
		perkenalan = "Bu " + nama + " adalah seorang " + pekerjaan + " yang berusia " + umur + " tahun"
	}
	return
}

// Soal 3
func buahFavorit(nama string, buahBuahan ...string) (kenal string) {
	var buah2 = []string{}
	for _, buah := range buahBuahan {
		buah2 = append(buah2, buah)
	}
	kenal = "Halo nama saya " + nama + " dan buah favorit saya adalah "

	for i := 0; i < len(buah2); i++ {

		if i == len(buah2)-1 {
			kenal += "\"" + buah2[i] + "\""
		} else {
			kenal += "\"" + buah2[i] + "\", "
		}
	}

	return
}

func main() {
	panjang := 12
	lebar := 4
	tinggi := 8

	luas := luasPersegiPanjang(panjang, lebar)
	keliling := kelilingPersegiPanjang(panjang, lebar)
	volume := volumeBalok(panjang, lebar, tinggi)

	fmt.Println(luas)
	fmt.Println(keliling)
	fmt.Println(volume)

	john := introduce("John", "laki-laki", "penulis", "30")
	fmt.Println(john) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

	sarah := introduce("Sarah", "perempuan", "model", "28")
	fmt.Println(sarah) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	var buah = []string{"semangka", "jeruk", "melon", "pepaya"}

	var buahFavoritJohn = buahFavorit("John", buah...)

	fmt.Println(buahFavoritJohn)
	// halo nama saya john dan buah favorit saya adalah "semangka", "jeruk", "melon", "pepaya"

	// Soal 4
	var dataFilm = []map[string]string{}
	// buatlah closure function disini
	var tambahDataFilm = func(nama string, durasi string, genre string, tahun string) {
		film := make(map[string]string)
		film["genre"] = genre
		film["jam"] = durasi
		film["tahun"] = tahun
		film["title"] = nama
		dataFilm = append(dataFilm, film)
	}

	tambahDataFilm("LOTR", "2 jam", "action", "1999")
	tambahDataFilm("avenger", "2 jam", "action", "2019")
	tambahDataFilm("spiderman", "2 jam", "action", "2004")
	tambahDataFilm("juon", "2 jam", "horror", "2004")

	for _, item := range dataFilm {
		fmt.Println(item)
	}
}
