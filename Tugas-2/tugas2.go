package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// Soal 1
	kata1 := "Bootcamp"
	kata2 := "Digital"
	kata3 := "Skill"
	kata4 := "Sanbercode"
	kata5 := "Golang"
	jawaban1 := kata1 + " " + kata2 + " " + kata3 + " " + kata4 + " " + kata5

	fmt.Println(jawaban1)

	// Soal 2
	halo := "Halo Dunia"
	jawaban2 := strings.Replace(halo, "Dunia", "Golang", 1)

	fmt.Println(jawaban2)

	// Soal 3
	var kataPertama = "saya"
	var kataKedua = "senang"
	var kataKetiga = "belajar"
	var kataKeempat = "golang"
	jawaban3 := kataPertama + " " + strings.Replace(kataKedua, "senang", "Senang", 1) + " " + strings.Replace(kataKetiga, "belajar", "belajaR", 1) + " " + strings.ToUpper(kataKeempat)

	fmt.Println(jawaban3)

	// Soal 4
	var angkaPertama = "8"
	var angkaKedua = "5"
	var angkaKetiga = "6"
	var angkaKeempat = "7"
	intPertama, _ := strconv.ParseInt(angkaPertama, 10, 16)
	intKedua, _ := strconv.ParseInt(angkaKedua, 10, 16)
	intKetiga, _ := strconv.ParseInt(angkaKetiga, 10, 16)
	intKeempat, _ := strconv.ParseInt(angkaKeempat, 10, 16)

	fmt.Println(intPertama + intKedua + intKetiga + intKeempat)

	// Soal 5
	kalimat := "halo halo bandung"
	angka := 2021
	gantiKalimat := strings.Replace(kalimat, "halo", "Hi", 2)

	fmt.Println("\""+gantiKalimat+"\" - ", angka)
}
