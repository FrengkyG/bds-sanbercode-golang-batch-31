package main

import (
	"fmt"
	"strconv"
)

func main() {
	// Soal 1
	var panjangPersegiPanjang string = "8"
	var lebarPersegiPanjang string = "5"
	var alasSegitiga string = "6"
	var tinggiSegitiga string = "7"
	var luasPersegiPanjang int
	var kelilingPersegiPanjang int
	var luasSegitiga int

	convPanjangPersegiPanjang, _ := strconv.Atoi(panjangPersegiPanjang)
	convLebarPersegiPanjang, _ := strconv.Atoi(lebarPersegiPanjang)
	convAlasSegitiga, _ := strconv.Atoi(alasSegitiga)
	convTinggiSegitiga, _ := strconv.Atoi(tinggiSegitiga)

	luasPersegiPanjang = convPanjangPersegiPanjang + convLebarPersegiPanjang
	kelilingPersegiPanjang = 2 * (convPanjangPersegiPanjang + convLebarPersegiPanjang)
	luasSegitiga = (convAlasSegitiga * convTinggiSegitiga) / 2

	fmt.Println("Luas Persegi Panjang = ", luasPersegiPanjang)
	fmt.Println("Keliling Persegi Panjang = ", kelilingPersegiPanjang)
	fmt.Println("Luas Segitiga = ", luasSegitiga)

	// Soal 2
	var nilaiJohn = 80
	var nilaiDoe = 50

	if nilaiJohn >= 80 {
		fmt.Println("Indeks nilai John adalah A")
	} else if nilaiJohn >= 70 && nilaiJohn < 80 {
		fmt.Println("Indeks nilai John adalah B")
	} else if nilaiJohn >= 60 && nilaiJohn < 70 {
		fmt.Println("Indeks nilai John adalah C")
	} else if nilaiJohn >= 50 && nilaiJohn < 60 {
		fmt.Println("Indeks nilai John adalah D")
	} else {
		fmt.Println("Indeks nilai John adalah E")
	}

	if nilaiDoe >= 80 {
		fmt.Println("Indeks nilai John adalah A")
	} else if nilaiDoe >= 70 && nilaiDoe < 80 {
		fmt.Println("Indeks nilai John adalah B")
	} else if nilaiDoe >= 60 && nilaiDoe < 70 {
		fmt.Println("Indeks nilai John adalah C")
	} else if nilaiDoe >= 50 && nilaiDoe < 60 {
		fmt.Println("Indeks nilai John adalah D")
	} else {
		fmt.Println("Indeks nilai John adalah E")
	}

	// Soal 3
	var tanggal = 19
	var bulan = 11
	var tahun = 1991

	switch bulan {
	case 1:
		fmt.Println(tanggal, "Januari", tahun)
	case 2:
		fmt.Println(tanggal, "Februari", tahun)
	case 3:
		fmt.Println(tanggal, "Maret", tahun)
	case 4:
		fmt.Println(tanggal, "April", tahun)
	case 5:
		fmt.Println(tanggal, "Mei", tahun)
	case 6:
		fmt.Println(tanggal, "Juni", tahun)
	case 7:
		fmt.Println(tanggal, "Juli", tahun)
	case 8:
		fmt.Println(tanggal, "Agustus", tahun)
	case 9:
		fmt.Println(tanggal, "September", tahun)
	case 10:
		fmt.Println(tanggal, "Oktober", tahun)
	case 11:
		fmt.Println(tanggal, "November", tahun)
	case 12:
		fmt.Println(tanggal, "Desember", tahun)
	}

	// Soal 4
	if tahun >= 1944 && tahun <= 1964 {
		fmt.Println("Aku adalah Baby boomer")
	} else if tahun >= 1965 && tahun <= 1979 {
		fmt.Println("Aku adalah Generasi X")
	} else if tahun >= 1980 && tahun <= 1994 {
		fmt.Println("Aku adalah Generasi Generasi Y (Millenials)")
	} else if tahun >= 1995 && tahun <= 2015 {
		fmt.Println("Aku adalah Generasi Generasi Z")
	} else {
		fmt.Println("Aku lahir dibawah tahun 1944 atau di atas tahun 2015")
	}

}
